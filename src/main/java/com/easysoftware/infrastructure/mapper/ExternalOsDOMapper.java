package com.easysoftware.infrastructure.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.easysoftware.infrastructure.externalos.gatewayimpl.dataobject.ExternalOsDO;

public interface ExternalOsDOMapper extends BaseMapper<ExternalOsDO> {

}
