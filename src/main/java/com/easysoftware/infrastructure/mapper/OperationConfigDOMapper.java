package com.easysoftware.infrastructure.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.easysoftware.infrastructure.operationconfig.gatewayimpl.dataobject.OperationConfigDO;

public interface OperationConfigDOMapper extends BaseMapper<OperationConfigDO> {
}
