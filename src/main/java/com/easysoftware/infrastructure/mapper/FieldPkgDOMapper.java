package com.easysoftware.infrastructure.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.easysoftware.infrastructure.fieldpkg.dataobject.FieldPkgDO;

public interface FieldPkgDOMapper extends BaseMapper<FieldPkgDO> {
}
