package com.easysoftware.infrastructure.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.easysoftware.infrastructure.applicationversion.gatewayimpl.dataobject.ApplicationVersionDO;

public interface ApplicationVersionDOMapper extends BaseMapper<ApplicationVersionDO> {

}
