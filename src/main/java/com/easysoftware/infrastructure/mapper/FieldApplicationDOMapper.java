package com.easysoftware.infrastructure.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.easysoftware.infrastructure.fieldapplication.gatewayimpl.dataobject.FieldApplicationDO;

public interface FieldApplicationDOMapper extends BaseMapper<FieldApplicationDO> {

}
