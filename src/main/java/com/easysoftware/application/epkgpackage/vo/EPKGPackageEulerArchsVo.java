package com.easysoftware.application.epkgpackage.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EPKGPackageEulerArchsVo {
    /**
     * OpenEuler arch of the package.
     */
    private String arch;
}
