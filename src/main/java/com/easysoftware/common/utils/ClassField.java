package com.easysoftware.common.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public final class ClassField {


    // Private constructor to prevent instantiation of the utility class
    private ClassField() {
        // private constructor to hide the implicit public one
        throw new AssertionError("ClassField class cannot be instantiated.");
    }

    /**
     * Retrieve the field names of an object.
     *
     * @param obj The object for which to retrieve field names
     * @return A list of field names as strings
     */
    public static List<String> getFieldNames(final Object obj) {
        List<String> columnNames = new ArrayList<>();

        Class<?> clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();

        for (Field field : fields) {
            if (List.class.isAssignableFrom(field.getType())) {
                continue;
            }
            String fieldName = field.getName();
            String columnName = fieldName.replaceAll("([A-Z])", "_$1").toLowerCase();
            columnNames.add(columnName);
        }

        return columnNames;
    }
}
